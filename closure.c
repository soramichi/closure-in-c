#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

typedef struct{
  int size;
  void* addr;
  char* value;
} bind;

typedef struct{
  int n_binds;
  bind* environment;
  void (*function)();
} closure;

// make_closure(function, number of variables to bind, size of the 1st variable, address of the 1st variable, ...)
closure make_closure(void (*f)(), int N, ...){
  closure ans;
  va_list ap;
  int i;

  ans.function = f;
  ans.environment = malloc(sizeof(bind) * N);
  ans.n_binds = N;

  va_start(ap, N);

  // bind the variables to the current values
  for(i=0;i<N;i++){
    ans.environment[i].size = va_arg(ap, int);
    ans.environment[i].addr = va_arg(ap, void*);
    ans.environment[i].value = malloc(ans.environment[i].size);

    memcpy(ans.environment[i].value, ans.environment[i].addr, ans.environment[i].size);
  }

  va_end(ap);

  return ans;
}

void call(closure cl){
  int i;
  char** tmp = malloc(sizeof(char*) * cl.n_binds);

  // restore the variables to the point when the closure was created
  for(i=0;i<cl.n_binds;i++){
    tmp[i] = malloc(cl.environment[i].size);
    memcpy(tmp[i], cl.environment[i].addr, cl.environment[i].size);
    memcpy(cl.environment[i].addr, cl.environment[i].value, cl.environment[i].size);
  }

  cl.function();

  // restore the variables to the point when this function was called
  for(i=0;i<cl.n_binds;i++){
    memcpy(cl.environment[i].addr, tmp[i], cl.environment[i].size);
  }
}

main(){
  int i,j;
  closure array1[10];
  void (*array2[10])();

  for(i=0; i<10; i++){
    void g(){
      printf("%d\n", i);
    }

    // arguments: function, number of variables to bind, size of the 1st variable, address of the 1st variable, ...
    array1[i] = make_closure(g, 1, 4, &i);

    //
    array2[i] = g;
  }

  // Example of calling the closure
  // Note that i is changed since the closures were created
  for(i=100; i<110; i++){
    // yields 0, 1, 2, ..., 9
    call(array1[i-100]);
  }

  // Here it yields 100, 101, 102, ..., 109 because i is changed by the for loop
  for(i=100; i<110; i++){
    array2[i-100]();
  }
}
